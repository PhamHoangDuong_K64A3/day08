<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

session_start();
$_POST = $_SESSION;
?>

<html>

<head>
    <meta charset="UTF-8">
    <title>day08 - confirm</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container submitted">
        <form method="post" class="form-wrapper">
            <div>
                <label for="name" class="form-label">Họ và tên</label>
                <label class="form-value"><?= $_POST["name"]; ?></label>
            </div>
            <div>
                <label for="gender" class="form-label">Giới tính</label>
                <label class="form-value"><?= $genders[$_POST["gender"]]; ?></label>
            </div>
            <div class="select-wrapper">
                <label for="faculty" class="form-label">Phân khoa</label>
                <label class="form-value"><?= $faculties[$_POST["faculty"]]; ?></label>
            </div>
            <div>
                <label for="birthday" class="form-label">Ngày sinh</label>
                <label class="form-value"><?= $_POST["birthday"]; ?></label>
            </div>
            <div>
                <label for="address" class="form-label">Địa chỉ</label>
                <label class="form-value"><?= $_POST["address"]; ?></label>
            </div>
            <div class="picture-wrapper">
                <label for="picture" class="form-label">Hình ảnh</label>
                <label class="form-value">
                    <img src="<?= $_POST["picture"]; ?>" alt="Hình ảnh" class="form-image">
                </label>
            </div>
            <input type="submit" value="Xác nhận" name="submit" class="submit-btn" />
        </form>
    </div>

</body>

</html>